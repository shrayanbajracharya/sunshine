package com.example.android.sunshine.ui

import com.example.android.sunshine.data.local.WeatherDao
import com.example.android.sunshine.data.local.WeatherDatabase

abstract class BaseRepository {

    fun getWeatherDaoInstance(): WeatherDao {
        return WeatherDatabase.getDatabaseInstance().weatherDao()
    }
}