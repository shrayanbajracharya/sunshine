package com.example.android.sunshine.ui.home

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.android.sunshine.R
import com.example.android.sunshine.data.ui.WeatherUi
import com.example.android.sunshine.databinding.ActivityMainBinding

class HomeActivity : AppCompatActivity() {

    /**
     * Views in MainActivity
     */
    private lateinit var imgWeatherCondition: ImageView

    private lateinit var binding: ActivityMainBinding
    
    private val viewModel by lazy{
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        mappingViews()

        viewModel.getWeatherLiveData().observe(this, Observer { weatherInfo ->
            displayData(weatherInfo)
        })

        viewModel.fetchAndStoreWeatherInfo()
    }

    private fun displayData(weatherInfo: WeatherUi?) {
        binding.weatherUi = weatherInfo
        Glide.with(this)
            .load(weatherInfo!!.icon)
            .into(imgWeatherCondition)
    }

    private fun mappingViews() {
        imgWeatherCondition = findViewById(R.id.img_weather_icon)
    }
}
