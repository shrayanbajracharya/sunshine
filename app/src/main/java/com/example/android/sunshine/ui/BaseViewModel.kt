package com.example.android.sunshine.ui

import androidx.lifecycle.ViewModel
import com.example.android.sunshine.network.Api
import com.example.android.sunshine.network.RetrofitClient

abstract class BaseViewModel : ViewModel() {

    fun getNetworkClient(): Api {
        return RetrofitClient.getApiInstance()
    }
}