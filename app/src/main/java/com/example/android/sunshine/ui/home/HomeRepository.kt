package com.example.android.sunshine.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.sunshine.data.local.WeatherEntity
import com.example.android.sunshine.ui.BaseRepository
import com.example.android.sunshine.utils.AppUtils
import org.jetbrains.anko.doAsync

class HomeRepository private constructor() : BaseRepository() {

    companion object {
        private var instance: HomeRepository? = null

        fun getInstance(): HomeRepository {
            return instance ?: HomeRepository()
        }
    }

    private val weatherDao = getWeatherDaoInstance()
    private val weatherEntityLiveData by lazy {
        MutableLiveData<WeatherEntity>()
    }

    init {
        /**
         * Reading weather info from database
         */
        doAsync {
            val weatherEntity = weatherDao.getWeatherInfoFor(AppUtils.LOCATION)
                ?: return@doAsync // If no result in database return

            // Posting returned data from database to LiveData
            weatherEntityLiveData.postValue(weatherEntity)
        }
    }

    /**
     * Pass/Return LiveData for WeatherEntity
     */
    fun getWeatherLiveData(): MutableLiveData<WeatherEntity> {
        Log.v("Repo", "Data Read")
        return weatherEntityLiveData
    }

    /**
     * Insert into Database (WeatherEntity)
     */
    fun insertWeatherEntity(weatherEntity: WeatherEntity) {
        doAsync {
            weatherDao.insertWeatherInfoIntoDatabase(weatherEntity)
        }
        Log.v("Repo", "Data inserted")
    }

    /**
     * Delete all from Database
     */
    fun deleteWeatherEntity() {
        doAsync {
            weatherDao.deleteWeatherInfoFromDatabase()
        }
    }
}