package com.example.android.sunshine.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.android.sunshine.data.local.WeatherEntity
import com.example.android.sunshine.data.remote.ResponseWrapper
import com.example.android.sunshine.data.remote.response.WeatherResponse
import com.example.android.sunshine.data.ui.WeatherUi
import com.example.android.sunshine.ui.BaseViewModel
import com.example.android.sunshine.utils.AppUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.RoundingMode
import java.text.DecimalFormat

class HomeViewModel : BaseViewModel() {

    /**
     * Members
     */
    private val repository by lazy {
        HomeRepository.getInstance()
    }
    private val _weatherEntity by lazy {
        repository.getWeatherLiveData()
    }

    /**
     * Transforming LiveData from WeatherEntity to WeatherUi
     * using Transformations.map
     */
    private val _weatherUI =
        Transformations.map<WeatherEntity, WeatherUi>(_weatherEntity, ::transformEntityToUi)

    private var responseWrapper: ResponseWrapper<WeatherResponse> = ResponseWrapper()

    /**
     * Returns LiveData of WeatherUi to MainActivity to be observed and displayed
     */
    fun getWeatherLiveData(): LiveData<WeatherUi> {
        return _weatherUI
    }

    /**
     * Network Request
     */
    fun fetchAndStoreWeatherInfo() {

        if (AppUtils.isNotConnectedToInternet()) {
            return // If no internet, fetch latest database data and return
        }

        /**
         * Making network request and storing latest data
         */
        getNetworkClient()
            .getWeatherResponse(AppUtils.LOCATION, AppUtils.UNITS, AppUtils.API_KEY)
            .enqueue(object : Callback<WeatherResponse> {
                override fun onResponse(
                    call: Call<WeatherResponse>,
                    response: Response<WeatherResponse>
                ) {
                    responseWrapper.isSuccessful = response.isSuccessful
                    responseWrapper.responseBody = response.body()

                    if (responseWrapper.isNotSuccessful()) { // If request not successful, return
                        responseWrapper.errorMessage = "Error fetching data from server."
                        return
                    }

                    responseWrapper.errorMessage = "Response successfully received from server"

                    repository.insertWeatherEntity(
                        transformResponseToEntity(responseWrapper.getResponse())
                    )
                }

                override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                    responseWrapper.isSuccessful = false
                    responseWrapper.errorMessage = t.localizedMessage
                    Log.d("kgj", t.localizedMessage)
                }
            })
    }

    /**
     * Transforming Response to Entity
     */
    private fun transformResponseToEntity(weatherResponse: WeatherResponse?): WeatherEntity {

        val weatherEntity = WeatherEntity()

        val decimalFormat = DecimalFormat("##")
        decimalFormat.roundingMode = RoundingMode.CEILING

        if (weatherResponse != null) {
            weatherEntity.location = weatherResponse.name

            weatherEntity.weatherCondition = weatherResponse.weather[0].main
            weatherEntity.weatherDescription = weatherResponse.weather[0].description

            weatherEntity.temperature = decimalFormat
                .format(weatherResponse.main.temp)
                .toString()
            weatherEntity.minTemperature = decimalFormat
                .format(weatherResponse.main.tempMin)
                .toString()
            weatherEntity.maxTemperature = decimalFormat
                .format(weatherResponse.main.tempMax)
                .toString()

            weatherEntity.imageUri = getIconFromURI(weatherResponse.weather[0].icon)
        }
        return weatherEntity
    }

    /**
     * Transforming Entity to Ui
     */
    private fun transformEntityToUi(weatherEntity: WeatherEntity): WeatherUi {

        val weatherUi = WeatherUi()

        val degreeSymbol = "\u00B0"

        weatherUi.location = weatherEntity.location

        weatherUi.weatherCondition = weatherEntity.weatherCondition
        weatherUi.weatherDescription = weatherEntity.weatherDescription

        weatherUi.temperature = weatherEntity.temperature + degreeSymbol

        weatherUi.minTemperature = weatherEntity.minTemperature + degreeSymbol
        weatherUi.maxTemperature = weatherEntity.maxTemperature + degreeSymbol

        weatherUi.icon = weatherEntity.imageUri

        return weatherUi
    }

    /**
     * Retrieving icon for Weather Condition
     */
    private fun getIconFromURI(icon: String): String {
        return String.format("https://api.openweathermap.org/img/w/%s.png", icon)
    }
}