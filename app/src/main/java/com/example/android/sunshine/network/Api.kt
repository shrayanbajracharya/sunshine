package com.example.android.sunshine.network

import com.example.android.sunshine.data.remote.response.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Methods in API
 */
interface Api {

    @GET("weather")
    fun getWeatherResponse(
        @Query("q") location: String,
        @Query("units") units: String,
        @Query("APPID") apiKey: String
    ): Call<WeatherResponse>

}