package com.example.android.sunshine.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Data Access Object
 */
@Dao
interface WeatherDao {

    @Query("SELECT * FROM weather_table")
    fun getWeatherInfoFromDatabase(): WeatherEntity

    @Query("SELECT * FROM weather_table WHERE location = :location")
    fun getWeatherInfoFor(location: String): WeatherEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeatherInfoIntoDatabase(weatherEntity: WeatherEntity)

    @Query("DELETE FROM weather_table")
    fun deleteWeatherInfoFromDatabase()
}