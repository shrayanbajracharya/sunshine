package com.example.android.sunshine.data.remote

/**
 * Response Wrapper - Wraps result from a network request
 * with corresponding
 * Error Message and
 * Status
 */
data class ResponseWrapper<O>(
    var isSuccessful: Boolean = false,
    var errorMessage: String = "",
    var responseBody: O? = null
) {

    fun getResponse(): O? {
        return responseBody
    }

    fun isNotSuccessful(): Boolean {
        return !isSuccessful || responseBody == null
    }

}